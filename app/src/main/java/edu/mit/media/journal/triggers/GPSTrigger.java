package edu.mit.media.journal.triggers;

import android.content.Context;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.R;
import edu.mit.media.journal.triggering_manager.TriggeringManager;


public class GPSTrigger extends MyTrigger {

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationListener location_listener;
    private Location current_location;
    private GoogleApiClient_callbacks google_api_client_callbacks;
    private long time_in_current_location = 0; //in milliseconds
    private static final String DEBUG_TAG = GPSTrigger.class.getSimpleName();
    Logger logger;
    Context context;
    Resources res;


    public class GoogleApiClient_callbacks implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        GPSTrigger invoking_trigger;

        public GoogleApiClient_callbacks(GPSTrigger trigger){
            this.invoking_trigger = trigger;
        }

        @Override
        public void onConnected(Bundle connectionHint) {
            Log.d(DEBUG_TAG, "GPSTrigger Connected to google play services");
            Location initial_location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (null != initial_location){
                current_location = initial_location;
                Log.d(DEBUG_TAG, String.format("Got initial location %s", initial_location.toString()));
            }

            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, location_listener);
        }

        @Override
        public void onConnectionSuspended(int cause) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, location_listener);
            logger.fine(String.format("Google location services suspended no location monitoring: cause %d",cause));
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.d(DEBUG_TAG, "GPSTrigger Could not connect to google play services");
            logger.fine("Google location services could not be connected - no location monitoring");
        }

        public void destruction() {
            if (googleApiClient != null){
                if (location_listener != null){
                    LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, location_listener);
                }
            }
        }

    }

    public class LocationListener_callbacks implements LocationListener {
        GPSTrigger invoking_trigger;

        public LocationListener_callbacks(GPSTrigger trigger) {
            this.invoking_trigger = trigger;
        }

        @Override
        public void onLocationChanged(Location location) {
            if (location == null){
                return;
            }

            //Log.d(DEBUG_TAG, String.format("GPSTrigger Location updated to: %s",location.toString()));

            if (current_location == null) {
                time_in_current_location = 0;
                current_location = location;
                return;
            }
            // Test if the location is out of the radius of the previous location
            if (res.getInteger(R.integer.GPSTrigger_minimal_radius) > location.distanceTo(current_location)){
                //If it is in the radius - update the estimated location with the new location point (weighted by the time spent in the location)
                long time_elapsed =  location.getTime() - current_location.getTime();
                double avg_lat = (location.getLatitude()*time_elapsed + current_location.getLatitude()*time_in_current_location)/(time_elapsed+time_in_current_location);
                double avg_lon = (location.getLongitude()*time_elapsed + current_location.getLongitude()*time_in_current_location)/(time_elapsed+time_in_current_location);

                current_location.setLatitude(avg_lat);
                current_location.setLongitude(avg_lon);
                current_location.setTime(location.getTime());
                time_in_current_location += time_elapsed;

            } else {
                //If it isn't - test to see how long we are in this location.
                if (res.getInteger(R.integer.GPSTrigger_minimal_time_in_location) < time_in_current_location) {
                    // Enough time was spent in location
                    invoking_trigger.create_and_queue_trigger(TriggerType.GPS_TRIGGER);
                    logger.info(String.format("GPS trigger queuing trigger - location changed from %s to %s",current_location.toString(),location.toString()));
                    //TODO I should print here the name / address of the location that was just left.
                } else {
                    logger.info(String.format("GPSTrigger, not triggering because not enough time was spent in location - only %d", time_in_current_location));
                }
                //Update location anyway
                time_in_current_location = 0;
                current_location = location;
            }
        }
    }


    public GPSTrigger(Context _context, TriggeringManager triggering_manager) {

        super(_context, triggering_manager);
        context = _context;
        res = context.getResources();
        logger = LogManager.getLogManager().getLogger(context.getResources().getString(R.string.service_logger_name));

        google_api_client_callbacks = new GoogleApiClient_callbacks(this);
        this.googleApiClient= new GoogleApiClient.Builder(_context)
            .addConnectionCallbacks(google_api_client_callbacks)
            .addOnConnectionFailedListener(google_api_client_callbacks)
            .addApi(LocationServices.API)
            .build();

        this.locationRequest= LocationRequest.create();
        this.locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY); //Notice: defines the power consumption, try also PRIORITY_HIGH_ACCURACY for better accuracy
        this.locationRequest.setInterval(res.getInteger(R.integer.GPSTrigger_location_request_update_interval)); //Setting interval to 5 min
        this.locationRequest.setFastestInterval(res.getInteger(R.integer.GPSTrigger_location_maximal_update)); //Declaring I can handle updates every 5 min

        this.location_listener = new LocationListener_callbacks(this);

        Log.d(DEBUG_TAG, "GPSTrigger initiated");
        if (this.googleApiClient!= null) {
            this.googleApiClient.connect();
        } else {
            logger.fine("Could not retrieve google api client - no location monitoring");
        }
    }

    public void destruction() {
        if (google_api_client_callbacks != null) {
            google_api_client_callbacks.destruction();
        }

        if (this.googleApiClient != null){
            this.googleApiClient.disconnect();
        }
    }
}
