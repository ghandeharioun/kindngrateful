package edu.mit.media.journal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class DemoActivity extends Activity {
    private static final String DEBUG_TAG = DemoActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "Demo notification is sent");

        Intent request_notification_intent = new Intent(this, TriggersMonitoringService.class);
        request_notification_intent.setAction("edu.mit.media.journal.DemoActivity_request_notification");
        this.startService(request_notification_intent);
        this.finish();
    }
}
