package edu.mit.media.journal.triggering_manager;

import android.os.Bundle;

import edu.mit.media.journal.triggers.TriggerType;

public class TriggeringEvent {
    Bundle event_data = new Bundle();

    public TriggeringEvent(TriggerType trigger_type) {
        this.event_data.putInt("trigger_type", trigger_type.ordinal());
        this.event_data.putLong("timestamp", System.currentTimeMillis());
    }
}
