package edu.mit.media.journal;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES;

public class EnableDiscoveryActivity extends Activity {
    private static final String DEBUG_TAG = EnableDiscoveryActivity.class.getSimpleName();
    BluetoothAdapter bluetooth_adapter = null;
    int number_of_attempts;
    Resources res;
    Logger logger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = this.getResources();
        logger = LogManager.getLogManager().getLogger(this.getResources().getString(R.string.service_logger_name));

        Log.d(DEBUG_TAG, "EnableDiscoveryActivity activity was created");

        number_of_attempts = res.getInteger(R.integer.BTTrigger_number_of_enable_discovery_attempts);

        Intent intent = getIntent();
        if (intent==null) {
            this.finish();
            return;
        }
        String action = intent.getAction();

        if (!("edu.mit.media.journal.EnableDiscoveryActivity_request".equals(action))) {
            Log.w(DEBUG_TAG, "EnableDiscoveryActivity received a message other than _request_");
            this.finish();
            return;
        }

        bluetooth_adapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetooth_adapter == null) {
            logger.severe("BTTrigger device does not support Bluetooth :( ");
            this.finish();
            return;
            // NOTICE here we do not tell the service to continue as there is no point...
        }

        if (test_and_set_discoverable_mode()) {
            Log.d(DEBUG_TAG, "BTTrigger Device is discoverable!");
            notify_service_to_continue();
            this.finish();
        } else {
            Log.d(DEBUG_TAG, "BTTrigger Device is not discoverable - user was prompted to enable");
        }
    }

    public Boolean test_and_set_discoverable_mode() {
        int bt_state = bluetooth_adapter.getScanMode();
        Log.d(DEBUG_TAG, String.format("BTTrigger Device state is: %d",bt_state));
        if (bt_state == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE){
            number_of_attempts = res.getInteger(R.integer.BTTrigger_number_of_enable_discovery_attempts);
            return true;
        }

        if (number_of_attempts > 0) {
            Intent discoverable_intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverable_intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0); // 0 means we want to set it ON indefinitely
            this.startActivityForResult(discoverable_intent, MAIN_MSGS_CODES.MSG_ENABLE_BT_DISCOVERY_REQUEST.ordinal());
            number_of_attempts--;
        } else {
            logger.fine("BTTrigger exceeded number of attempts to prompt the user - giving up");
            return true;
            // NOTICE: we return true here just so that we will at least scan for others even though they may not see us.
            }
        return false;
    }

    public void notify_service_to_continue(){
        Intent discovery_result_intent = new Intent(this, TriggersMonitoringService.class);
        discovery_result_intent.setAction("edu.mit.media.journal.EnableDiscoveryActivity_result");
        this.startService(discovery_result_intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode != MAIN_MSGS_CODES.MSG_ENABLE_BT_DISCOVERY_REQUEST.ordinal()) {
            return;
        }

        if (!test_and_set_discoverable_mode()) {
            Log.d(DEBUG_TAG, "BTTrigger user did not make the device discoverable, trying again");
            return;
        }

        notify_service_to_continue();
        this.finish();
    }
}

