package edu.mit.media.journal.triggers;

public enum TriggerType {
    BLUETOOTH_TRIGGER,
    GPS_TRIGGER,
    CALENDAR_TRIGGER,
    ACTIVITY_TRIGGER,
    AMBIENT_NOISE_TRIGGER,
    PERIODIC_TRIGGER
}
