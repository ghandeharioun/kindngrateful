package edu.mit.media.journal.triggers;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.util.Log;

import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.EnableDiscoveryActivity;
import edu.mit.media.journal.R;
import edu.mit.media.journal.triggering_manager.TriggeringManager;

public class BluetoothTrigger extends MyTrigger {
    BluetoothAdapter bluetooth_adapter = null;
    Service context;
    TriggeringManager triggering_manager;
    Boolean[] address_seen_during_scan;
    Boolean[] address_seen_before_scan;
    ScheduledFuture discovery_scheduling_handler;
    Resources res;
    Logger logger;

    private static final String DEBUG_TAG = BluetoothTrigger.class.getSimpleName();


    public BluetoothTrigger(Service _context, TriggeringManager _triggering_manager) {
		super(_context, _triggering_manager);
        Log.d(DEBUG_TAG, "BTTrigger initializing trigger class");

        context = _context;
        triggering_manager = _triggering_manager;
        bluetooth_adapter = BluetoothAdapter.getDefaultAdapter();
        res = context.getResources();
        logger = LogManager.getLogManager().getLogger(context.getResources().getString(R.string.service_logger_name));

        String[] bt_members_addresses = res.getStringArray(R.array.BTTrigger_group_members_bluetooth_addresses);
        address_seen_during_scan = new Boolean[bt_members_addresses.length];
        Arrays.fill(address_seen_during_scan, Boolean.FALSE);
        address_seen_before_scan = new Boolean[bt_members_addresses.length];
        Arrays.fill(address_seen_before_scan , Boolean.FALSE);

        if (bluetooth_adapter == null) {
            logger.severe("BTTrigger device does not support Bluetooth :( ");
            return;
        }

        start_enable_discovery_activity();
	}

    public void start_enable_discovery_activity(){
        Intent start_discovery_activity_intent = new Intent(context, EnableDiscoveryActivity.class);
        start_discovery_activity_intent.setAction("edu.mit.media.journal.EnableDiscoveryActivity_request");
        start_discovery_activity_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(start_discovery_activity_intent);
        Log.d(DEBUG_TAG,"Starting activity to enable bluetooth discovery");
    }

    public void enable_discovery_activity_result_handler(){
        Log.d(DEBUG_TAG,"Enable bluetooth discovery activity is done");
        register_periodic_discovery();
    }

    public int is_monitored_group_member(BluetoothDevice device){
        if (device==null){
            return -1;
        }
        Log.d(DEBUG_TAG, String.format("BTTrigger found device with name %s and address %s",device.getName(),device.getAddress()));
        Resources res = context.getResources();
        String[] members_addresses = res.getStringArray(R.array.BTTrigger_group_members_bluetooth_addresses);
        return Arrays.asList(members_addresses).indexOf(device.getAddress());
    }

    public void delayed_enqueue_of_bluetooth_trigger_event() {
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        final Runnable enqueue_bt_trigger_event = new Runnable() {
            public void run() {
                logger.info("BTTrigger queueing trigger after delay");
                create_and_queue_trigger(TriggerType.BLUETOOTH_TRIGGER);
            }
        };
        scheduler.schedule(enqueue_bt_trigger_event, res.getInteger(R.integer.BTTrigger_delay_after_not_seeing_member), TimeUnit.MINUTES);
    }

    public Boolean start_discovery() {
        Log.d(DEBUG_TAG, "BTTrigger starting discovery");
        if (bluetooth_adapter == null){
            logger.severe("BT adapter is null when starting discovery");
            return false;
        }
        if (bluetooth_adapter.isDiscovering()){
            Log.d(DEBUG_TAG, "BTTrigger someone else is already discovering - wait for next time to try scanning");
            return false;
        }
        final BroadcastReceiver discovery_receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent == null){
                    Log.d(DEBUG_TAG,"BT discovery broadcast receiver received null intent");
                    return;
                }
                String action = intent.getAction();
                // When discovery finds a device
                //TODO: test if this should be in a separate broadcast receiver object
                if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                    Log.d(DEBUG_TAG, "BTTrigger got DISCOVERY_STARTED event");
                    Arrays.fill(address_seen_during_scan, Boolean.FALSE);
                    return;
                }

                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    Log.d(DEBUG_TAG, "BTTrigger got ACTION_FOUND event");
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    int member_index = is_monitored_group_member(device);
                    if (member_index == -1) {
                        return;
                    }
                    Log.d(DEBUG_TAG, String.format("BTTrigger found monitored member device in index: %d", member_index));
                    //TODO:log the device address that was identified.
                    address_seen_during_scan[member_index] = Boolean.TRUE;
                    return;
                }

                if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    Log.d(DEBUG_TAG, "BTTrigger got DISCOVERY_FINISHED event");
                    bluetooth_adapter.cancelDiscovery();
                    context.unregisterReceiver(this);

                    for (int index=0; index<address_seen_during_scan.length; index++){
                        if ((!address_seen_during_scan[index]) && (address_seen_before_scan[index])){
                            logger.info(String.format("BTTrigger device in index %d disappeared - initiating delayed queueing of trigger", index));
                            delayed_enqueue_of_bluetooth_trigger_event();
                        }
                    }
                    address_seen_before_scan = Arrays.copyOf(address_seen_during_scan,address_seen_during_scan.length);
                }
            }
        };

        // Register the BroadcastReceiver
        IntentFilter discovery_filter = new IntentFilter();
        discovery_filter.addAction(BluetoothDevice.ACTION_FOUND);
        discovery_filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        discovery_filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        context.registerReceiver(discovery_receiver, discovery_filter);

        if (!bluetooth_adapter.startDiscovery()){
            context.unregisterReceiver(discovery_receiver);
            Log.d(DEBUG_TAG, "BTTrigger could not start discovery");
            return false;
        }
        Log.d(DEBUG_TAG, "BTTrigger discovery started");
        return true;
    }

    private void register_periodic_discovery(){
        if (discovery_scheduling_handler != null){
            discovery_scheduling_handler.cancel(true);
        }
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        final Runnable execute_discovery = new Runnable() {
            public void run() {
                start_discovery();
            }
        };
        discovery_scheduling_handler = scheduler.scheduleAtFixedRate(execute_discovery,
                res.getInteger(R.integer.BTTrigger_discovery_scheduling_period),
                res.getInteger(R.integer.BTTrigger_discovery_scheduling_period),
                TimeUnit.MINUTES);
        Log.d(DEBUG_TAG, "BTTrigger periodic discovery is scheduled");
    }
}

