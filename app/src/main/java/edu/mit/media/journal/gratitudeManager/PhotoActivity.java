package edu.mit.media.journal.gratitudeManager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.R;
import edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES;

public class PhotoActivity extends GratitudeActionActivity{
    private Logger logger;
    private String photo_path;
    private Uri photo_uri;
    ImageView mImageView;
    private static final String PHOTO_FOLDER="/kindnGrateful/";


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        logger = LogManager.getLogManager().getLogger(this.getResources().getString(R.string.activity_logger_name));

		setContentView(R.layout.activity_send_photo);
        register_peer_spinner();
        set_activity_fonts();

        mImageView = (ImageView) findViewById(R.id.photo_img);

        generate_file_path();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photo_uri);
        startActivityForResult(intent, MAIN_MSGS_CODES.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE.ordinal());
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MAIN_MSGS_CODES.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE.ordinal()) {
            if (resultCode == RESULT_OK) {
                Bitmap image_bitmap = BitmapFactory.decodeFile(photo_path);
                Bitmap thumb = ThumbnailUtils.extractThumbnail(image_bitmap, 384, 512);
                mImageView.setImageBitmap(thumb);
                return;
            }
            this.finish();
        }
    }

    private void generate_file_path(){

        //here,we are making a folder named picFolder to store pics taken by the camera using this application
        final String directory_path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + PHOTO_FOLDER;
        File directory_obj = new File(directory_path);
        directory_obj.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        photo_path = directory_path + timeStamp;
        photo_path = photo_path + ".jpg";
        File file_obj = new File(photo_path);
        photo_uri = Uri.fromFile(file_obj);
    }

	public void close(View v){
        logger.info("GratitudeActivity:PhotoActivity: user canceled sending a photo.");
        this.finish();
	}
	
	public void send(View v){
        EditText msg_body = (EditText)findViewById(R.id.msg_body_editText);
        Intent photo_intent = new Intent(Intent.ACTION_SEND);
        photo_intent.putExtra(Intent.EXTRA_TEXT, msg_body.getText().toString());
        photo_intent.putExtra(Intent.EXTRA_STREAM, photo_uri);
        photo_intent.setType("image/jpg");

        startActivity(Intent.createChooser(photo_intent, "Send"));

        logger.info("GratitudeActivity:PhotoActivity: user sent a photo - " + " to_peer: " + to_peer + " body: " + msg_body.getText().toString() + " photo_path: " + photo_path);
		this.finish();
		
	}
}
