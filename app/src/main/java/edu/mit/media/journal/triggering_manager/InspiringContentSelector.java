package edu.mit.media.journal.triggering_manager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.R;

enum ContentSelectorTypes {
    IMAGE1,
    IMAGE2,
    QUOTE,
}

public class InspiringContentSelector {
    Context context;
    private Random random_generator = new Random();
    private static final String DEBUG_TAG = InspiringContentSelector.class.getSimpleName();
    Logger logger;
    public String content_path = "";
    public int quote_index = 0;


    public InspiringContentSelector(Context _context) {
        context = _context;
        logger = LogManager.getLogManager().getLogger(context.getResources().getString(R.string.service_logger_name));
    }

    public int select_random_quote(){
        Resources res = context.getResources();
        String[] quotes = res.getStringArray(R.array.inspiring_quotes);
        quote_index = random_generator.nextInt(quotes.length);
        return quote_index;
    }

    public String get_random_quote(){
        Resources res = context.getResources();
        String[] quotes = res.getStringArray(R.array.inspiring_quotes);
        return quotes[quote_index];
    }

    public String select_random_image_path(){
        try {
            String[] directory_list = context.getAssets().list("InspiringMaterialThumbs");
            String chosen_directory = directory_list[random_generator.nextInt(directory_list.length)];
            String[] files_list = context.getAssets().list("InspiringMaterialThumbs/" + chosen_directory );
            String chosen_file = files_list[random_generator.nextInt(files_list.length)];

            Log.d(DEBUG_TAG, String.format("these are the folders %s", Arrays.toString(directory_list)));
            Log.d(DEBUG_TAG, String.format("Chose folder %s", chosen_directory));
            Log.d(DEBUG_TAG, String.format("these are the files %s", Arrays.toString(files_list)));
            Log.d(DEBUG_TAG, String.format("Chose file %s", chosen_file));

            content_path = chosen_directory + "/" + chosen_file;
            return content_path;

        } catch (IOException e) {
            e.printStackTrace();
            logger.severe("Got IO exception when accessing inspiring content assets");
            content_path = "";
            return content_path;
        }

    }

    public Bitmap get_random_notification_image(){
        try {
            InputStream image_stream = context.getAssets().open("InspiringMaterialThumbs/" + content_path);
            return BitmapFactory.decodeStream(image_stream);
        } catch (IOException e) {
            e.printStackTrace();
            logger.severe("Got IO exception when reading thumb image as bitmap");
            content_path = "";
            return null;
        }
    }
    public Bitmap get_random_wear_image(){
        try {
            InputStream image_stream = context.getAssets().open("InspiringMaterialWear/" + content_path);
            return BitmapFactory.decodeStream(image_stream);
        } catch (IOException e) {
            e.printStackTrace();
            logger.severe("Got IO exception when reading image as bitmap");
            content_path = "";
            return null;
        }
    }

    public ContentSelectorTypes select_content_type(){
        //return ContentSelectorTypes.IMAGE;
        return ContentSelectorTypes.values()[random_generator.nextInt(ContentSelectorTypes.values().length)];
    }


}
