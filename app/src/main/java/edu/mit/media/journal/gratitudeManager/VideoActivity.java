package edu.mit.media.journal.gratitudeManager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.R;
import edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES;

public class VideoActivity extends GratitudeActionActivity {
    private Logger logger;
    private String video_path;
    private Uri video_uri;
    ImageView mImageView;
    private static final String VIDEO_FOLDER="/kindnGrateful/";

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        logger = LogManager.getLogManager().getLogger(this.getResources().getString(R.string.activity_logger_name));

		setContentView(R.layout.activity_send_video);
        register_peer_spinner();
        set_activity_fonts();

        mImageView= (ImageView) findViewById(R.id.video_img);

        generate_file_path();
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, video_uri);
        startActivityForResult(intent, MAIN_MSGS_CODES.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE.ordinal());
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MAIN_MSGS_CODES.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE.ordinal()){
            if (resultCode == RESULT_OK) {
                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Images.Thumbnails.MINI_KIND);
                mImageView.setImageBitmap(thumb);
                return;
            }
            this.finish();
        }
    }

    private void generate_file_path(){

        //here,we are making a folder named picFolder to store pics taken by the camera using this application
        final String directory_path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + VIDEO_FOLDER;
        File directory_obj = new File(directory_path);
        directory_obj.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        video_path = directory_path + timeStamp;
        video_path = video_path + ".mp4";
        File file_obj = new File(video_path);
        video_uri = Uri.fromFile(file_obj);
    }

	public void close(View v){
        logger.info("GratitudeActivity:VideoActivity: user canceled sending a photo.");
        this.finish();
	}
	
	public void send(View v){
        EditText msg_body=(EditText)findViewById(R.id.msg_body_editText);

        Intent video_intent= new Intent(Intent.ACTION_SEND);
        video_intent.putExtra(Intent.EXTRA_TEXT, msg_body.getText().toString());
        video_intent.putExtra(Intent.EXTRA_STREAM, video_uri);
        video_intent.setType("video/mp4");

        startActivity(Intent.createChooser(video_intent,"Send"));

        logger.info("GratitudeActivity:VideoActivity: user sent a video - " + " to_peer: "+ to_peer + " body: "+ msg_body.getText().toString() + " photo_path: " + video_path);
        this.finish();
    }
    }
