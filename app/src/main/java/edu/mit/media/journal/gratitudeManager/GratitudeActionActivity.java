package edu.mit.media.journal.gratitudeManager;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import edu.mit.media.journal.R;

public class GratitudeActionActivity extends Activity implements AdapterView.OnItemSelectedListener{
    private static final String DEBUG_TAG = GratitudeActionActivity .class.getSimpleName();
    String to_peer = "";
    Resources res;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = this.getResources();
    }

    public void register_peer_spinner(){
        to_peer = res.getStringArray(R.array.Gratitude_manager_members_names)[0];
        Spinner members_list = (Spinner) findViewById(R.id.member);
        members_list.setOnItemSelectedListener(this);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.Gratitude_manager_members_names, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        members_list.setAdapter(adapter);
    }

    public void set_activity_fonts(){
        TextView headline = (TextView) findViewById(R.id.headline);
        TextView msg_body = (TextView) findViewById(R.id.msg_body_editText);
        TextView to_peer_question = (TextView) findViewById(R.id.addressed_to_text);
        Button send = (Button) findViewById(R.id.buttonSend);
        Button cancel = (Button) findViewById(R.id.buttonCancel);

        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/Helvetica.ttf");

        headline.setTypeface(tf);
        msg_body.setTypeface(tf);
        to_peer_question.setTypeface(tf);
        send.setTypeface(tf);
        cancel.setTypeface(tf);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
        to_peer = (String)parent.getItemAtPosition(pos);
        Log.d(DEBUG_TAG, String.format("GratitudeActivity:MessageActivity: user selected from to peer dropdown: %s", to_peer));
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }
}
