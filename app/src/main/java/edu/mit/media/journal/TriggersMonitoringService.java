package edu.mit.media.journal;

import android.app.Service;
import android.content.Intent;
import android.content.res.Resources;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.triggering_manager.InspiringNotification;
import edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES;
import edu.mit.media.journal.triggering_manager.TriggeringManager;
import edu.mit.media.journal.triggers.ActivityTrigger;
import edu.mit.media.journal.triggers.BluetoothTrigger;
import edu.mit.media.journal.triggers.GPSTrigger;
import edu.mit.media.journal.triggers.PeriodicTriggering;
import edu.mit.media.journal.triggers.TriggerType;

import static edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES.MSG_INVALID_MSG;
import static edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES.values;

public class TriggersMonitoringService extends Service {
    TriggeringManager triggering_manager;
    BluetoothTrigger bluetoothTrigger;
    GPSTrigger gpsTrigger;
    ActivityTrigger activityTrigger;
    PeriodicTriggering periodicTrigger;
    Resources res;

    ScheduledFuture triggering_manager_scheduling_handler = null;

    private static final String DEBUG_TAG = TriggersMonitoringService.class.getSimpleName();
    Logger logger;

    @Override
    public void onCreate() {
        super.onCreate();
        res = this.getResources();
        triggering_manager_scheduling_handler = null;

        //String logger_name = res.getString(R.string.logger_name);
        //String log_file_name = res.getString(R.string.log_file_name);
        //String crash_path = res.getString(R.string.crash_file_name);

        InternalLogger.initialize_logger(res, res.getString(R.string.service_logger_name), res.getString(R.string.logs_folder), res.getString(R.string.service_log_file_name), res.getString(R.string.logs_folder));
        logger = LogManager.getLogManager().getLogger(res.getString(R.string.service_logger_name));
        logger.fine("TriggerMonitoringService was created");

        triggering_manager = new TriggeringManager(this);

        if ("CONTEXTUAL".equals(res.getString(R.string.TriggeringService_triggering_mode))){
            bluetoothTrigger = new BluetoothTrigger(this, triggering_manager);
            gpsTrigger = new GPSTrigger(this, triggering_manager);
            activityTrigger = new ActivityTrigger(this, triggering_manager);
        } else {
            if ("PERIODIC".equals(res.getString(R.string.TriggeringService_triggering_mode))) {
                periodicTrigger = new PeriodicTriggering(this,triggering_manager);
            } else {
                logger.severe("Invalid triggering mode! nothing will trigger!");
                return;
            }
        }

        Log.d(DEBUG_TAG, "TriggerMonitoringService - triggering manager and individual triggers initialized");

        register_periodic_triggering_manager_events_handling();

    }




    private void register_periodic_triggering_manager_events_handling(){
        if (triggering_manager_scheduling_handler != null){
            if (!(triggering_manager_scheduling_handler.isDone())) {
                Log.d(DEBUG_TAG , "Triggering manager is still scheduled - ignoring request to schedule");
                return;
            }
        }

        ScheduledExecutorService triggering_manager_scheduler = Executors.newScheduledThreadPool(1);
        final Runnable handle_triggering_events = new Runnable() {
            public void run() {
                //Log.d(DEBUG_TAG,"handling triggers");
                triggering_manager.handle_triggers();
            }
        };

        triggering_manager_scheduling_handler = triggering_manager_scheduler.scheduleAtFixedRate(handle_triggering_events,
                res.getInteger(R.integer.TriggersMonitorService_handle_triggers_period),
                res.getInteger(R.integer.TriggersMonitorService_handle_triggers_period),
                TimeUnit.SECONDS);
        Log.d(DEBUG_TAG, "TriggeringMonitorService periodic triggers handling is scheduled");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null){
            Log.d(DEBUG_TAG,"received null intent");
            return Service.START_STICKY;
        }

        String action = intent.getAction();
        Log.d(DEBUG_TAG, String.format("TriggeringMonitorService received action on start command: %s", action));

        if ("edu.mit.media.journal.EnableDiscoveryActivity_result".equals(action)) {
            Log.d(DEBUG_TAG , "TriggerMonitoringService - received an answer from BTTrigger enable discovery");
            bluetoothTrigger.enable_discovery_activity_result_handler();
        }

        if ("edu.mit.media.journal.TriggerMonitoringService_start".equals(action)) {
            Log.d(DEBUG_TAG, "TriggerMonitoringService - main activity is running and made sure the service is running");
        }

        if ("edu.mit.media.journal.Periodic_trigger_alarm_intent_to_service".equals(action)){
            Log.d(DEBUG_TAG,"TriggerMonitoringService - received periodic trigger alarm");
            periodicTrigger.handle_periodic_trigger();
        }

        if ("edu.mit.media.journal.ActivityRecognition_result".equals(action)) {
            Log.d(DEBUG_TAG, "TriggerMonitoringService - received activity detection notification");
            activityTrigger.handle_activity_recognition_result_intent(intent);
        }

        if ("edu.mit.media.journal.DemoActivity_request_notification".equals(action)) {
            Log.d(DEBUG_TAG, "Demo activity requested notification");
            gpsTrigger.create_and_queue_trigger(TriggerType.GPS_TRIGGER);
        }

        if (Intent.ACTION_VIEW.equals(action)) {
            handle_notification_intents(intent);
        }

        return Service.START_STICKY;
    }

    public void handle_notification_intents(Intent intent) {
        String type = intent.getType();
        if (type == null) {
            logger.severe("Notification intent arrived with null type");
            return;
        }

        MAIN_MSGS_CODES msg_code = values()[intent.getIntExtra("main_msg_code", MSG_INVALID_MSG.ordinal())];
        switch (msg_code) {
            case MSG_IGNORED_QUOTE:
                int ignored_quote_index = intent.getIntExtra("quote_index", 0);
                logger.info(String.format("User Ignored quote notification - index: %d", ignored_quote_index));
                break;
            case MSG_LIKE_QUOTE:
                int liked_quote_index = intent.getIntExtra("quote_index", 0);
                logger.info(String.format("User Liked quote notification - index: %d", liked_quote_index));
                break;
            case MSG_IGNORED_IMAGE:
                String ignored_image_path = intent.getParcelableExtra(Intent.EXTRA_STREAM).toString();
                logger.info(String.format("User Ignored image notification - uri: %s", ignored_image_path));
                break;
            case MSG_LIKE_IMAGE:
                String liked_image_path = intent.getParcelableExtra(Intent.EXTRA_STREAM).toString();
                logger.info(String.format("User Liked image notification - uri: %s", liked_image_path));
                break;
            default:
                logger.severe(String.format("Received notification intent with unsupported msg code %s", msg_code));
        }

        InspiringNotification.cancel_notification(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Not allowing to bind the service therefore we return here null
        return null;
    }

    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        if (gpsTrigger != null) {
            gpsTrigger.destruction();
        }

        if (activityTrigger != null) {
            activityTrigger.destruction();
        }
        logger.fine("TriggeringMonitorService was destroyed :(");
    }


}

