package edu.mit.media.journal.triggering_manager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.R;

public class TriggeringManager {
    Context context;
    InspiringContentSelector content_selector;
    public InspiringNotification inspiring_notification;
    Queue<TriggeringEvent> triggering_events_queue;
    long last_notification_timestamp;
    private static final String DEBUG_TAG = TriggeringManager.class.getSimpleName();
    Logger logger;
    Resources res;

    public  TriggeringManager(Context _context) {
        context=_context;
        content_selector = new InspiringContentSelector(context);
        inspiring_notification = new InspiringNotification(context);
        triggering_events_queue = new LinkedList<TriggeringEvent>();
        last_notification_timestamp = 0;
        logger = LogManager.getLogManager().getLogger(context.getResources().getString(R.string.service_logger_name));
        res = context.getResources();
    }

    public Boolean enqueue_trigger(TriggeringEvent event){
        // This limits the size of the queue so that it will not explode. It should never really be more than the maximum.
        while (triggering_events_queue.size() >= context.getResources().getInteger(R.integer.TriggerManager_maximum_triggers_in_queue)){
            Log.d(DEBUG_TAG, "Triggering manager queue contains more than maximum number of notifications");
            triggering_events_queue.poll();
        }

        if ("INTERFACE_ONLY".equals(res.getString(R.string.TriggeringService_app_mode))){
            logger.info(String.format("Triggering manager - trigger occurred with type %d and timestamp %s",event.event_data.getInt("trigger_type"),event.event_data.getLong("timestamp")));
            return true;
        }

        if ("INTERFACE_AND_INSPIRE".equals(res.getString(R.string.TriggeringService_app_mode))) {
            if (!triggering_events_queue.offer(event)){
                logger.severe("Triggering manager could not write to the queue");
                return false;
            }

            logger.info(String.format("Triggering manager - trigger occurred with type %d and timestamp %s",event.event_data.getInt("trigger_type"),event.event_data.getLong("timestamp")));
            return true;
        }

        logger.severe("TriggeringManager: app mode is set to invalid value!!!");
        return false;
    }

    public void empty_old_events_from_queue() {
        if (triggering_events_queue.isEmpty()) {
            //Log.d(DEBUG_TAG,"queue is empty nothing to empty");
            return;
        }
        long current_time = System.currentTimeMillis();
        Boolean events_are_current = false;
        while (!events_are_current) {
            TriggeringEvent triggering_event = triggering_events_queue.peek();
            // discarding expired events from the queue
            if ((current_time - triggering_event.event_data.getLong("timestamp")) > res.getInteger(R.integer.TriggerManager_event_expired_threshold)) {
                TriggeringEvent expired_event = triggering_events_queue.poll();
                logger.info(String.format("Triggering manager - discarded expired event with type %d and timestamp %s", expired_event.event_data.getInt("trigger_type"), expired_event.event_data.getLong("timestamp")));
            } else {
                events_are_current = true;
            }
        }
    }

    public void generate_notification(){
        ContentSelectorTypes content_type = content_selector.select_content_type();
        switch (content_type) {
            case QUOTE:
                int random_quote_index = content_selector.select_random_quote();
                String random_quote = content_selector.get_random_quote();
                logger.info(String.format("Triggering manager - Notifying the user with quote in index %d: %s",random_quote_index,random_quote));
                inspiring_notification.generate_quote_notification(random_quote, random_quote_index);
                break;
            case IMAGE1:
            case IMAGE2:
                String random_path = content_selector.select_random_image_path();
                Bitmap notification_random_image = content_selector.get_random_notification_image();
                Bitmap wear_random_image = content_selector.get_random_wear_image();
                logger.info(String.format("Triggering manager - Notifying the user with image in path: %s",random_path));
                inspiring_notification.generate_image_notification(notification_random_image, wear_random_image, random_path);
        }
    }

    public void handle_triggers() {
        if (triggering_events_queue.isEmpty()) {
            //Log.d(DEBUG_TAG, "queue is empty");
            return;
        }

        empty_old_events_from_queue();

        long current_time = System.currentTimeMillis();
        Log.d(DEBUG_TAG, String.format("current time %s last notification %s, min time %s", current_time, last_notification_timestamp, res.getInteger(R.integer.TriggerManager_minimal_time_between_notifications)));
        if ((current_time - last_notification_timestamp) < res.getInteger(R.integer.TriggerManager_minimal_time_between_notifications)) {
            Log.d(DEBUG_TAG,"not enough time since last notification");
            return;
        }

        TriggeringEvent triggering_event = triggering_events_queue.poll();
        logger.info(String.format("Triggering manager - the following event generated a user notification: type %d and timestamp %s",
                triggering_event.event_data.getInt("trigger_type"),
                triggering_event.event_data.getLong("timestamp")));
        generate_notification();
        last_notification_timestamp = current_time;
    }
}
