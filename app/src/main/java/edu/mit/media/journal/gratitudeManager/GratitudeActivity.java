package edu.mit.media.journal.gratitudeManager;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.InternalLogger;
import edu.mit.media.journal.R;
import edu.mit.media.journal.TriggersMonitoringService;
import edu.mit.media.journal.triggering_manager.InspiringNotification;
import edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES;

import static edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES.MSG_INVALID_MSG;

public class GratitudeActivity extends Activity {

    private Logger logger;
    private static final String DEBUG_TAG = GratitudeActivity.class.getSimpleName();
    Resources res;

    private ImageView inspirationalImg;
    private TextView inspirationalTxt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        res = this.getResources();

        logger = LogManager.getLogManager().getLogger(this.getResources().getString(R.string.activity_logger_name));
        if (logger == null) {
            Log.d(DEBUG_TAG,"Activity logger is not initialized - initializing logger");
            InternalLogger.initialize_logger(res, res.getString(R.string.activity_logger_name), res.getString(R.string.logs_folder), res.getString(R.string.activity_log_file_name), res.getString(R.string.logs_folder));
            logger = LogManager.getLogManager().getLogger(this.getResources().getString(R.string.activity_logger_name));
        }

        setContentView(R.layout.activity_gratitude);
        inspirationalImg=(ImageView)findViewById(R.id.inspirational_img);
        inspirationalTxt=(TextView)findViewById(R.id.inspirational_quote);
		setFonts();

        //getting the notification intent
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_MAIN.equals(action)) {
            Log.d(DEBUG_TAG, "Received Android ACTION_MAIN - Attempting to restart the service in case it is down");
            Intent start_monitoring_service_intent = new Intent(this, TriggersMonitoringService.class);
            start_monitoring_service_intent.setAction("edu.mit.media.journal.TriggerMonitoringService_start");
            this.startService(start_monitoring_service_intent);

            try {
                InputStream image_stream = this.getAssets().open("InspiringMaterial/Gratitude/elephant_helping_dog.jpg");
                Bitmap image_bitmap = BitmapFactory.decodeStream(image_stream);
                inspirationalImg.setImageBitmap(image_bitmap);
            } catch (IOException e) {
                inspirationalImg.setImageResource(android.R.color.transparent);
                logger.severe("Could not open landing page image");
                e.printStackTrace();
            }
            inspirationalTxt.setText(res.getString(R.string.IWant2));
        }

        if (Intent.ACTION_VIEW.equals(action)) {
            if (type == null) {
                logger.fine("Received a VIEW action no defined type");
                return;
            }


            inspirationalImg.setImageResource(android.R.color.transparent);
            inspirationalTxt.setText("");

            MAIN_MSGS_CODES msg_code = MAIN_MSGS_CODES.values()[intent.getIntExtra("main_msg_code", MSG_INVALID_MSG.ordinal())];
            switch (msg_code) {
                case MSG_EXPRESS_IMAGE:
                case MSG_PRESSED_IMAGE:
                    if (msg_code.ordinal() == MAIN_MSGS_CODES.MSG_EXPRESS_IMAGE.ordinal()){
                        logger.info("GratitudeActivity - user pressed Express for Image");
                    }
                    if (msg_code.ordinal() == MAIN_MSGS_CODES.MSG_PRESSED_IMAGE.ordinal()){
                        logger.info("GratitudeActivity - user pressed Image");
                    }
                    if (type.startsWith("image")) {
                        try {
                            Uri uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                            String image_path = uri.getPath().replaceFirst("/android_asset/","InspiringMaterial/");
                            Log.d(DEBUG_TAG, String.format("Displaying the user with image %s",image_path));

                            InputStream image_stream = this.getAssets().open(image_path);
                            Bitmap image_bitmap = BitmapFactory.decodeStream(image_stream);
                            inspirationalImg.setImageBitmap(image_bitmap);

                        } catch (IOException e) {
                            e.printStackTrace();
                            logger.severe("Got IO exception when reading image received from the notification as bitmap");
                        }
                    }
                    else {
                        logger.severe("Received an image notification with wrong type");
                    }
                    break;

                case MSG_EXPRESS_QUOTE:
                case MSG_PRESSED_QUOTE:
                    if (msg_code.ordinal() == MAIN_MSGS_CODES.MSG_EXPRESS_QUOTE.ordinal()){
                        logger.info("GratitudeActivity - user pressed Express for Quote");
                    }
                    if (msg_code.ordinal() == MAIN_MSGS_CODES.MSG_PRESSED_QUOTE.ordinal()){
                        logger.info("GratitudeActivity - user pressed Quote");
                    }

                    if (type.startsWith("text/plain")) {
                        String quote = intent.getStringExtra(Intent.EXTRA_TEXT);
                        Log.d(DEBUG_TAG, String.format("Displaying the user with a quote: %s", quote));

                        inspirationalTxt.setText(quote);
                    }
                    else {
                        logger.severe("Received a text notification with wrong type");
                    }
                    break;

                case MSG_INVALID_MSG:
                    logger.severe("Received a notification with MSG_INVALID_MSG");
                    return;
                default:
                    logger.severe("Received a notification with unsupported msg type");
                    return;
            }

            LinearLayout wholeView=(LinearLayout)findViewById(R.id.gratitude_activity);
            wholeView.invalidate();
            InspiringNotification.cancel_notification(this);

        }
	}

	public void sendMsg(View v){
		logger.info("GratitudeActivity - User pressed send msg button");
		Intent intent = new Intent(this, MessageActivity.class);
		startActivity(intent);
	}

	public void sendPhoto(View v){
        logger.info("GratitudeActivity - User pressed send photo button");
		Intent intent = new Intent(this, PhotoActivity.class);
		startActivity(intent);
	}

	public void sendVideo(View v){
        logger.info("GratitudeActivity - User pressed send video button");
        Intent intent = new Intent(this, VideoActivity.class);
        startActivity(intent);
	}

	public void sendGift(View v){
		logger.info("GratitudeActivity - User pressed send gift button");
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.amazon.com"));
		startActivity(browserIntent);
	}

	public void sendReport(View v){
		logger.info("GratitudeActivity - User pressed free report button");
		Intent intent = new Intent(this, FreeReportActivity.class);
		startActivity(intent);
	}

	private void setFonts(){
		Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/Helvetica.ttf");
		TextView tv = (TextView) findViewById(R.id.inspirational_quote);
		tv.setTypeface(tf);
	}


}
