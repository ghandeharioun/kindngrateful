package edu.mit.media.journal.gratitudeManager;

import edu.mit.media.journal.R;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.logging.LogManager;
import java.util.logging.Logger;

public class FreeReportActivity extends GratitudeActionActivity{
    private Logger logger;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        logger = LogManager.getLogManager().getLogger(this.getResources().getString(R.string.activity_logger_name));

		setContentView(R.layout.activity_send_report);
        register_peer_spinner();
        set_activity_fonts();
	}
	
	public void close(View v){
        logger.info("GratitudeActivity:FreeReportActivity - user canceled the free report");
        this.finish();
	}
	
	public void send(View v){
        EditText msg_body=(EditText)findViewById(R.id.msg_body_editText);
        logger.info("GratitudeActivity:FreeReportActivity - user reported on gratitude - " + " to_peer: "+ to_peer + " body: "+ msg_body.getText().toString());
        this.finish();
	}
}
