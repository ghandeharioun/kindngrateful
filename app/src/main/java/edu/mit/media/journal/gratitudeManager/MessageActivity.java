package edu.mit.media.journal.gratitudeManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import edu.mit.media.journal.R;

public class MessageActivity extends GratitudeActionActivity {
    private Logger logger;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        logger = LogManager.getLogManager().getLogger(this.getResources().getString(R.string.activity_logger_name));

        setContentView(R.layout.activity_send_msg);
        register_peer_spinner();
		set_activity_fonts();
	}

    public void close(View v){
        logger.info("GratitudeActivity:MessageActivity: user chose to cancel sending a text");
        this.finish();
	}
	
	public void send(View v){
        EditText txt=(EditText)findViewById(R.id.msg_body_editText);

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setData(Uri.parse("sms:"));
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, txt.getText().toString());
        logger.info("GratitudeActivity:MessageActivity: user sent a message - " + " to_peer: "+ to_peer + " body: "+ txt.getText().toString());
		startActivity(Intent.createChooser(sendIntent, "Send"));
		this.finish();
	}


}
