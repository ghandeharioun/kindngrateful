package edu.mit.media.journal.triggering_manager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;


import edu.mit.media.journal.R;
import edu.mit.media.journal.TriggersMonitoringService;
import edu.mit.media.journal.gratitudeManager.GratitudeActivity;

public class InspiringNotification {
    Context context;
    int small_icon = R.drawable.medialabicon;
    int large_icon = R.drawable.medialab_logo;

    public InspiringNotification(Context _context) {
        context = _context;
    }

    private PendingIntent build_image_intent(String asset_relative_path, Class destination_activity, int main_msg_code, int pending_intent_flag, Boolean use_stack_builder){
        Intent action_intent = new Intent(context, destination_activity);
        Uri image_uri = Uri.parse("file:///android_asset/" + asset_relative_path);
        action_intent.setAction(Intent.ACTION_VIEW);
        action_intent.putExtra(Intent.EXTRA_STREAM, image_uri);
        action_intent.putExtra("main_msg_code", main_msg_code);
        action_intent.setType("image/jpeg");

        PendingIntent pending_intent;

        if (use_stack_builder) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(destination_activity);
            stackBuilder.addNextIntent(action_intent);
            pending_intent = stackBuilder.getPendingIntent(main_msg_code, pending_intent_flag);
        } else {
            pending_intent = PendingIntent.getService(context, main_msg_code, action_intent, pending_intent_flag);
        }
        return pending_intent;
    }

    private PendingIntent build_quote_intent(int quote_index, String quote, Class destination_activity, int main_msg_code, int pending_intent_flag, Boolean use_stack_builder){
        Intent action_intent = new Intent(context, destination_activity);
        action_intent.setAction(Intent.ACTION_VIEW);
        action_intent.putExtra(Intent.EXTRA_TEXT, quote);
        action_intent.putExtra("quote_index", quote_index);
        action_intent.putExtra("main_msg_code", main_msg_code);
        action_intent.setType("text/plain");

        PendingIntent pending_intent;

        if (use_stack_builder) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(destination_activity);
            stackBuilder.addNextIntent(action_intent);
            pending_intent = stackBuilder.getPendingIntent(main_msg_code, pending_intent_flag);
        } else {
            pending_intent = PendingIntent.getService(context, main_msg_code, action_intent, pending_intent_flag);
        }
        return pending_intent;
    }

    public void generate_quote_notification(String quote_string, int quote_index){
        Bitmap large_icon_bitmap = BitmapFactory.decodeResource(context.getResources(), large_icon);
        NotificationCompat.Builder  notification_builder = new NotificationCompat.Builder(context)

                .setTicker("Lets inspire some gratitude!")
                .setContentTitle("Take a few moments to think")

                .setSmallIcon(small_icon)
                .setLargeIcon(large_icon_bitmap)

                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(quote_string))

                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)

                .setContentIntent(build_quote_intent(quote_index, quote_string, GratitudeActivity.class, MAIN_MSGS_CODES.MSG_PRESSED_QUOTE.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT, true))
                .addAction(small_icon, "Express yourself",
                        build_quote_intent(quote_index, quote_string, GratitudeActivity.class, MAIN_MSGS_CODES.MSG_EXPRESS_QUOTE.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT, true))
                .addAction(R.drawable.ic_action_good, "Like",
                        build_quote_intent(quote_index, quote_string, TriggersMonitoringService.class, MAIN_MSGS_CODES.MSG_LIKE_QUOTE.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT, false))
                .setDeleteIntent(build_quote_intent(quote_index, quote_string, TriggersMonitoringService.class, MAIN_MSGS_CODES.MSG_IGNORED_QUOTE.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT, false));
        //TODO: we need to figure out what flags exactly we should use here for each one

        int mNotificationId = 0x01;
        //TODO: decide whether we want the notifications to accumulate or run over each other
        NotificationManager notification_manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notification_manager.notify(mNotificationId, notification_builder.build());

    }

    public void generate_image_notification(Bitmap notification_inspiring_image, Bitmap wear_inspiring_image, String asset_relative_path){
        Bitmap large_icon_bitmap = BitmapFactory.decodeResource(context.getResources(), large_icon);

        Notification image_page = new NotificationCompat.Builder(context)
                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(wear_inspiring_image))
                .extend(new NotificationCompat.WearableExtender().setHintShowBackgroundOnly(true))
                .build();

        NotificationCompat.WearableExtender wear_extension = new NotificationCompat.WearableExtender()
                .addPage(image_page)
                .setBackground(wear_inspiring_image);

        NotificationCompat.Builder  notification_builder = new NotificationCompat.Builder(context)

                .setTicker("Lets inspire some gratitude!")
                .setContentTitle("Take a few moments")

                .setSmallIcon(small_icon)
                .setLargeIcon(large_icon_bitmap)
                .extend(wear_extension)

                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(notification_inspiring_image)
                        .bigLargeIcon(large_icon_bitmap))

                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)

                .setContentIntent(build_image_intent(asset_relative_path, GratitudeActivity.class, MAIN_MSGS_CODES.MSG_PRESSED_IMAGE.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT, true))
                .addAction(small_icon, "Express yourself",
                        build_image_intent(asset_relative_path, GratitudeActivity.class, MAIN_MSGS_CODES.MSG_EXPRESS_IMAGE.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT, true))
                .addAction(R.drawable.ic_action_good, "Like",
                        build_image_intent(asset_relative_path, TriggersMonitoringService.class, MAIN_MSGS_CODES.MSG_LIKE_IMAGE.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT, false))
                .setDeleteIntent(build_image_intent(asset_relative_path, TriggersMonitoringService.class, MAIN_MSGS_CODES.MSG_IGNORED_IMAGE.ordinal(), PendingIntent.FLAG_UPDATE_CURRENT, false));
                //TODO: we need to figure out what flags exactly we should use here for each one

        int mNotificationId = 0x01;
        NotificationManager notification_manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notification_manager.notify(mNotificationId, notification_builder.build());
    }

    public static void cancel_notification(Context context){
        int mNotificationId = 0x01;
        NotificationManager notification_manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notification_manager.cancel(mNotificationId);
    }
}
