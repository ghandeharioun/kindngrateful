package edu.mit.media.journal.triggers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.BootBroadcastReceiver;
import edu.mit.media.journal.R;
import edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES;
import edu.mit.media.journal.triggering_manager.TriggeringManager;

public class PeriodicTriggering extends MyTrigger{

    Service context;
    TriggeringManager triggering_manager;
    private static final String DEBUG_TAG = PeriodicTriggering.class.getSimpleName();
    Resources res;
    ScheduledFuture scheduling_handler;
    Logger logger;

    public PeriodicTriggering(Service _context, TriggeringManager _triggering_manager) {
        super(_context, _triggering_manager);
        Log.d(DEBUG_TAG, "Periodic triggering initializing trigger class");
        context = _context;
        triggering_manager = _triggering_manager;
        res = context.getResources();
        logger = LogManager.getLogManager().getLogger(context.getResources().getString(R.string.service_logger_name));

        register_periodic_triggering();
    }

    private void register_periodic_triggering(){

        AlarmManager alarmMgr;
        PendingIntent periodic_pending_intent;

        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent alarm_intent = new Intent(context, BootBroadcastReceiver.class);
        alarm_intent.setAction("edu.mit.media.journal.Periodic_trigger_alarm_intent");

        //Checking if the intent is already registered
        boolean is_alarm_registered = (PendingIntent.getBroadcast(context, MAIN_MSGS_CODES.PERIODIC_TRIGGER_INTENT.ordinal(), alarm_intent, PendingIntent.FLAG_NO_CREATE)!=null);

        if (is_alarm_registered) {
            Log.d(DEBUG_TAG, "PeriodicTrigger: Alarm already registered with pending intent - no need to register it again");
            return;
        }

        periodic_pending_intent = PendingIntent.getBroadcast(context, MAIN_MSGS_CODES.PERIODIC_TRIGGER_INTENT.ordinal(), alarm_intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                res.getInteger(R.integer.PeriodicTrigger_scheduling_period),
                res.getInteger(R.integer.PeriodicTrigger_scheduling_period),
                periodic_pending_intent);
        Log.d(DEBUG_TAG, "PeriodicTrigger: registered periodic trigger alarm");

    }

    public void handle_periodic_trigger(){
        logger.info("PeriodicTrigger: PeriodicTriggering queueing trigger");
        create_and_queue_trigger(TriggerType.PERIODIC_TRIGGER);
    }
    /*private void register_periodic_triggering(){
        if (scheduling_handler != null){
            scheduling_handler.cancel(true);
        }
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        final Runnable execute_discovery = new Runnable() {
            public void run() {
                logger.info("PeriodicTriggering queueing trigger");
                create_and_queue_trigger(TriggerType.PERIODIC_TRIGGER);
            }
        };
        scheduling_handler = scheduler.scheduleAtFixedRate(execute_discovery,
                res.getInteger(R.integer.PeriodicTrigger_scheduling_period),
                res.getInteger(R.integer.PeriodicTrigger_scheduling_period),
                TimeUnit.MINUTES);
        Log.d(DEBUG_TAG, "PeriodicTriggering periodic triggering is scheduled");
    } */
}
