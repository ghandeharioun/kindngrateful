package edu.mit.media.journal.triggers;

import android.content.Context;
import android.util.Log;

import edu.mit.media.journal.triggering_manager.TriggeringEvent;
import edu.mit.media.journal.triggering_manager.TriggeringManager;


public abstract class MyTrigger {
    Context context;
    TriggeringManager triggering_manager;
    private static final String DEBUG_TAG = MyTrigger.class.getSimpleName();

    public MyTrigger(Context _context, TriggeringManager _triggering_manager){
        context=_context;
        this.triggering_manager = _triggering_manager;
    }

    public void create_and_queue_trigger(TriggerType trigger_type){
        TriggeringEvent event = new TriggeringEvent(trigger_type);
        if (!this.triggering_manager.enqueue_trigger(event)) {
            Log.d(DEBUG_TAG, "MyTrigger could not enqueue trigger");
        }
    }

}
