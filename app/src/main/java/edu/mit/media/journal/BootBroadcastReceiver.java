package edu.mit.media.journal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent ==  null){
            return;
        }

        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent booting_intent = new Intent(context, TriggersMonitoringService.class);
            context.startService(booting_intent);
        }

        if ("edu.mit.media.journal.Periodic_trigger_alarm_intent".equals(intent.getAction())){
            Intent periodic_trigger_intent = new Intent(context, TriggersMonitoringService.class);
            periodic_trigger_intent.setAction("edu.mit.media.journal.Periodic_trigger_alarm_intent_to_service");
            context.startService(periodic_trigger_intent);
        }
    }
}
