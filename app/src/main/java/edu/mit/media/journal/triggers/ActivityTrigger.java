package edu.mit.media.journal.triggers;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import edu.mit.media.journal.R;
import edu.mit.media.journal.TriggersMonitoringService;
import edu.mit.media.journal.triggering_manager.MAIN_MSGS_CODES;
import edu.mit.media.journal.triggering_manager.TriggeringManager;

public class ActivityTrigger extends MyTrigger implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleApiClient googleApiClient;
    PendingIntent activity_recognition_result_pending_intent;
    private static final String DEBUG_TAG = ActivityTrigger.class.getSimpleName();
    Logger logger;
    Context context;
    Resources res;

    long time_spent_being_active;
    long time_spent_being_inactive;
    long last_update_timestamp;


    public ActivityTrigger(Context _context, TriggeringManager triggering_manager) {
        super(_context, triggering_manager);
        context = _context;
        res = context.getResources();
        logger = LogManager.getLogManager().getLogger(context.getResources().getString(R.string.service_logger_name));

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (this.googleApiClient != null) {
            Log.d(DEBUG_TAG, "ActivityTrigger: Connecting to GoogleAPI");
            this.googleApiClient.connect();
        } else {
            logger.fine("Could not retrieve google api client - no activity monitoring");
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(DEBUG_TAG, "ActivityTrigger: Connected to google play services");

        Intent activity_recognition_result_intent = new Intent(context, TriggersMonitoringService.class);
        activity_recognition_result_intent.setAction("edu.mit.media.journal.ActivityRecognition_result");
        activity_recognition_result_pending_intent = PendingIntent.getService(context, MAIN_MSGS_CODES.ACTIVITY_RECOGNITION_RESULT_MSG.ordinal(), activity_recognition_result_intent, PendingIntent.FLAG_UPDATE_CURRENT);


        last_update_timestamp = System.currentTimeMillis();
        time_spent_being_active = 0;
        time_spent_being_inactive = 0;

        ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(googleApiClient, activity_recognition_result_pending_intent);
        PendingResult pending_result = ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(this.googleApiClient, res.getInteger(R.integer.ActivityTrigger_update_frequency_request), activity_recognition_result_pending_intent);
        pending_result.setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status result) {
                Log.d(DEBUG_TAG, String.format("ActivityTrigger: the result of the updates_requests is %s, %s", result.getStatusCode(), result.getStatusMessage()));
            }
        });
        //TODO: test the return code for this request
    }


    @Override
    public void onConnectionSuspended(int cause) {
        ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(this.googleApiClient, this.activity_recognition_result_pending_intent);
        logger.fine(String.format("Google location services suspended no activity monitoring: cause %d",cause));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(DEBUG_TAG, "ActivityTrigger: Could not connect to google play services");
        logger.fine("Google location services could not be connected - no activity monitoring");
    }

    public void handle_activity_recognition_result_intent(Intent activity_result_intent){
        Log.d(DEBUG_TAG, "ActivityTrigger: Received activity update");

        if (!ActivityRecognitionResult.hasResult(activity_result_intent)){
            Log.d(DEBUG_TAG,"ActivityTrigger - received result intent without result");
            return;
        }

        ActivityRecognitionResult activity_result = ActivityRecognitionResult.extractResult(activity_result_intent);
        DetectedActivity most_probable_activity = activity_result.getMostProbableActivity();
        int confidence = most_probable_activity.getConfidence();

        Log.d(DEBUG_TAG, String.format("ActivityTrigger: received activity update - type %s confidence %d", convert_activity_type_to_string(most_probable_activity.getType()),confidence));

        if (confidence <=75){
            Log.d(DEBUG_TAG, "ActivityTrigger: confidence is lower than 75, ignoring activity update");
            return;
        }

        long update_timestamp = activity_result.getTime();

        switch (most_probable_activity.getType()) {
            case DetectedActivity.ON_BICYCLE:
            case DetectedActivity.ON_FOOT:
            case DetectedActivity.WALKING:
            case DetectedActivity.RUNNING:

                time_spent_being_active += update_timestamp - last_update_timestamp;
                time_spent_being_inactive = 0;
                last_update_timestamp = update_timestamp;
                Log.d(DEBUG_TAG, String.format("ActivityTrigger: current values are - active: %d, inactive: %d, last_update: %d", time_spent_being_active, time_spent_being_inactive, last_update_timestamp));
                if (time_spent_being_active >= res.getInteger(R.integer.ActivityTrigger_time_spent_active_threshold)){
                    this.create_and_queue_trigger(TriggerType.ACTIVITY_TRIGGER);
                    logger.info("ActivityTrigger: time spent active threshold exceeded - queuing trigger");
                    time_spent_being_active = 0;
                }
                break;

            case DetectedActivity.TILTING:
            case DetectedActivity.IN_VEHICLE:
            case DetectedActivity.STILL:
                Log.d(DEBUG_TAG, String.format("ActivityTrigger: current values are - active: %d, inactive: %d, last_update: %d", time_spent_being_active, time_spent_being_inactive, last_update_timestamp));
                time_spent_being_inactive += update_timestamp - last_update_timestamp;
                last_update_timestamp = update_timestamp;
                if (time_spent_being_inactive >= res.getInteger(R.integer.ActivityTrigger_time_spent_active_threshold)){
                    Log.d(DEBUG_TAG, "ActivityTrigger: spent more than threshold time inactive - resetting activity counting");
                    time_spent_being_active = 0;
                    time_spent_being_inactive = 0;
                }
                break;

            case DetectedActivity.UNKNOWN:
                Log.d(DEBUG_TAG,"ActivityTrigger: received UNKNOWN activity type - assuming it is the same as the next detected activity");
                break;

            default:
                logger.fine("ActivityTrigger: received an update with invalid detected type");
        }
    }

    public void destruction() {
        if (this.googleApiClient != null){
            if (this.activity_recognition_result_pending_intent != null) {
                ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(this.googleApiClient, this.activity_recognition_result_pending_intent);
            }
            this.googleApiClient.disconnect();
        }
    }

    private String convert_activity_type_to_string(int detected_activity){
        switch (detected_activity){
            case DetectedActivity.ON_BICYCLE:
                return "ON_BICYCLE";
            case DetectedActivity.ON_FOOT:
                return "ON_FOOT";
            case DetectedActivity.WALKING:
                return "WALKING";
            case DetectedActivity.RUNNING:
                return "RUNNING";
            case DetectedActivity.STILL:
                return "STILL";
            case DetectedActivity.TILTING:
                return "TILTING";
            case DetectedActivity.IN_VEHICLE:
                return "IN_VEHICLE";
            case DetectedActivity.UNKNOWN:
                return "UNKNOWN";
            default:
                return "INVALID";
        }
    }
}
