package edu.mit.media.journal;

import android.content.res.Resources;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class InternalLogger {
    private static final String DEBUG_TAG = InternalLogger .class.getSimpleName();

    public static void initialize_logger(Resources res, String logger_name, String logger_folder, String log_file_name, String crash_folder) {
    /*
        Notice:
            The code uses two types of loggers. The first is Log and the second is logger.
            Use Log for debug logs to follow the flow of the program and to test it.
            Use logger to write to the internal storage with the following tag conventions.

            INFO tag is for data that should later be analysed for the experiment.
            SEVERE tag is for internal errors that cause our program to to function.
            WARNING tag is for internal errors that we feel should be reported.
            FINE are for important stages of the app that need logging - for example - the device has been rebooted.

            logger should be accessible from anywhere as stated above. Logs that use logger
            are also logged into the console. The size of the logfile is limited to a constant.
        */

        LogManager log_manager = LogManager.getLogManager();
        String sd_state = Environment.getExternalStorageState();
        String log_folder_path = "";
        String crash_path = "";

        if (Environment.MEDIA_MOUNTED.equals(sd_state)) {
            log_folder_path = Environment.getExternalStorageDirectory().getAbsolutePath() + logger_folder;
            crash_path = Environment.getExternalStorageDirectory().getAbsolutePath() + crash_folder;
            Log.d(DEBUG_TAG, "Logger - sd is mounted");

        } else {
            log_folder_path= Environment.getDataDirectory().getAbsolutePath() + logger_folder;
            crash_path = Environment.getDataDirectory().getAbsolutePath() + crash_folder;
            Log.d(DEBUG_TAG, "Logger - sd is not mounted");
        }

        create_folder_if_does_not_exist(log_folder_path);
        create_folder_if_does_not_exist(crash_path);
        log_file_name = log_folder_path + "/" + log_file_name;
        Log.d(DEBUG_TAG, String.format("Logger is logging to file name %s", log_file_name));
        Log.d(DEBUG_TAG, String.format("Logger crash path is %s", crash_path));

        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(crash_path, logger_name));
        }

        try {
            FileHandler log_file_handler = new FileHandler(log_file_name,
                    res.getInteger(R.integer.TriggersMonitorService_logfile_maximum_size),
                    res.getInteger(R.integer.TriggersMonitorService_logfile_number_of_files),
                    true);

            log_file_handler.setFormatter(new SimpleFormatter());
            Logger logger_instance = Logger.getLogger(logger_name);
            logger_instance.addHandler(log_file_handler);
            log_manager.addLogger(logger_instance);

            logger_instance.log(Level.SEVERE, String.format("Logger %s has been initialized!",logger_name));
            log_file_handler.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void create_folder_if_does_not_exist(String folder_path){
        File folder = new File(folder_path);
        if (!folder.exists()){
            Log.d(DEBUG_TAG, "folder does not exist - creating it");
            folder.mkdir();
        }
    }

}
