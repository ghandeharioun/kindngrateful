package edu.mit.media.journal;

import android.content.res.Resources;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class CustomExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler defaultUEH;
    private String local_path;
    private String logger_name;
    private static final String DEBUG_TAG = CustomExceptionHandler.class.getSimpleName();

    public CustomExceptionHandler(String local_path, String logger_name) {
        this.local_path = local_path;
        this.logger_name = logger_name;
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
    }

    public void uncaughtException(Thread t, Throwable e) {
        Log.d(DEBUG_TAG, "Unhandled exception was caught - generating crash dump");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = dateFormat.format(new Date());

        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);

        e.printStackTrace(printWriter);
        String stack_trace = result.toString();
        printWriter.close();

        String file_name = this.logger_name + "_" + timestamp + ".stacktrace";
        writeToFile(stack_trace, file_name);
        defaultUEH.uncaughtException(t, e);
    }

    private void writeToFile(String stack_trace, String filename) {
        try {
            Log.d(DEBUG_TAG, String.format("Writing crash dump to file: %s", filename));
            BufferedWriter bos = new BufferedWriter(new FileWriter(local_path + "/" + filename));
            bos.write(stack_trace);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
